<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Language" content="cs">
    <meta name="created" content="Liquid Design s.r.o.">
    <link rel="shortcut icon" href="/jjtmpublic/favicon.ico">




    <!-- Font Awesome -->
    <link rel="stylesheet" href="public/node_modules/normalize.css/normalize.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="public/node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="public/node_modules/@fortawesome/fontawesome-free/css/all.css" rel="stylesheet" type="text/css">
    <link href="public/css/lightbox.min.css" rel="stylesheet" type="text/css">
    <link href="public/css/base.css" rel="stylesheet" type="text/css">
    <link href="public/css/front.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">


    <!--[if its IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- autorefresh
    <meta http-equiv="refresh" content="30" /> -->

</head>
<body>

<?php require 'menu.html'; ?>

<main class="bg-light">
    <div class="container">
        <div class="row">
            <nav aria-label="breadcrumb" class="col-12">
                <ol class="breadcrumb bg-transparent pl-0">
                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    <li class="breadcrumb-item active">New record</li>
                    <li class="breadcrumb-item active" aria-current="page">New record step 2.</li>
                </ol>
            </nav>
        </div>
        <div class="row">

            <div class="offset-md-2 col-md-4 border-top border-bottom text-center bg-success text-white rounded-left">
                <div class="position-absolute mt-3">
                    <i class="fas fa-check fa-2x"></i>
                </div>
                <div>
                    <strong> 1. Step</strong><br>
                    Required information
                </div>
            </div>


            <div class="col-md-4 border-top border-bottom text-center bg-color-primary rounded-right">
                <strong>2. Step</strong><br>
                Another Information
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="offset-md-2 col-md-8">
                    <h1 class="d-inline-block mr-1 mr-md-3">2/2 New record</h1>
                    <p>Budú požadované nepovinné informácie.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="bg-color-form pt-4 pb-4">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="offset-md-2 col-md-8">
                        <h2 class="mb-0">Another Information</h2>

                        <form>
                            <div class="bg-white border rounded p-3 mb-2">
                                <div class="form-group">
                                    <label for="typeSelect" class="custom-label-margin">
                                        <i data-toggle="tooltip" data-placement="top" title="Standard -  S7iFish support marks only; &nbsp; Homozygote – DNA sequence with nucleotide codes (A, T, G, C only); &nbsp; Heterozygote – DNA sequence with ambiguous IUPAC nucleotide codes (W, S, K, M, Y, R, etc.); &nbsp; Hybrid – DNA sequence with Hybrid Barcode – interspecific, intergeneric, etc. ">
                                            <u>Type sequence <i class="fas fa-info-circle"></i></u>
                                        </i>
                                    </label>
                                    <div class="row pl-3 pr-3">
                                        <div class="form-check form-check-inline border rounded pl-3 mr-1 mb-1 custom-radio-button">
                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                            <label class="form-check-label pt-2 pb-2 pr-3" for="inlineRadio1">Standard</label>
                                        </div>
                                        <div class="form-check form-check-inline border rounded pl-3 mr-1 mb-1 custom-radio-button">
                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                            <label class="form-check-label pt-2 pb-2 pr-3" for="inlineRadio2">Homozygote</label>
                                        </div>
                                        <div class="form-check form-check-inline border rounded pl-3 mr-1 mb-1 custom-radio-button">
                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3">
                                            <label class="form-check-label pt-2 pb-2 pr-3" for="inlineRadio3">Heterozygote</label>
                                        </div>
                                        <div class="form-check form-check-inline border rounded pl-3 mr-0 mb-1 custom-radio-button">
                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio4" value="option4">
                                            <label class="form-check-label pt-2 pb-2 pr-3" for="inlineRadio4">Hybrid</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label for="nuclearDiagnosticSelect" class="custom-label-margin">
                                        <i data-toggle="tooltip" data-placement="top" title="All – determination with unique JGM regions and CAPS assays; &nbsp; Indel – determination with unique JGM regions sufficient; &nbsp; Substitution – determinaton with CAPS assays only">
                                            <u>Nuclear diagnostics <i class="fas fa-info-circle"></i></u>
                                        </i>

                                    </label>
                                    <div class="row pl-3 pr-3">
                                        <div class="form-check form-check-inline border rounded pl-2 custom-radio-button">
                                            <input class="form-check-input" type="radio" name="Radio1" id="inlineRadio11" value="option1">
                                            <label class="form-check-label pt-2 pb-2 pr-2" for="inlineRadio11">All</label>
                                        </div>

                                        <div class="form-check form-check-inline border rounded pl-2 custom-radio-button">
                                            <input class="form-check-input" type="radio" name="Radio1" id="inlineRadio22" value="option2">
                                            <label class="form-check-label pt-2 pb-2 pr-2" for="inlineRadio22">Indel</label>
                                        </div>
                                        <div class="form-check form-check-inline border rounded pl-2 custom-radio-button">
                                            <input class="form-check-input" type="radio" name="Radio1" id="inlineRadio33" value="option3">
                                            <label class="form-check-label pt-2 pb-2 pr-2" for="inlineRadio33">Substitution</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label for="sexSelect" class="custom-label-margin">Sex</label>
                                    <div class="row pl-3 pr-3">
                                        <div class="form-check form-check-inline border rounded pl-2 custom-radio-button" id="">
                                            <input class="form-check-input" type="radio" name="sexSelect" id="sexSelect1" value="option1">
                                            <label class="form-check-label pt-2 pb-2 pr-2" for="sexSelect1">Male</label>
                                        </div>
                                        <div class="form-check form-check-inline border rounded pl-2 custom-radio-button" id="">
                                            <input class="form-check-input" type="radio" name="sexSelect" id="sexSelect2" value="option2">
                                            <label class="form-check-label pt-2 pb-2 pr-2" for="sexSelect2">Female</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="primaryTaxon" class="custom-label-margin">Sex specific marker</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control form-control-lg" id="primaryTaxon" placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="exampleFormControlFile1" class="custom-label-margin">Images (max. 4 images in format .jpg or .png)</label>
                                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                                </div>


                                <div class="form-group">
                                    <label for="primaryTaxon" class="custom-label-margin">GenBank Accession No</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control form-control-lg" id="collectors" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="primaryTaxon" class="custom-label-margin">Bold Sample ID</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control form-control-lg" id="collectors" placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="primaryTaxon" class="custom-label-margin">Deposited in</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control form-control-lg" id="collectors" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="primaryTaxon" class="custom-label-margin">Collector’s No</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control form-control-lg" id="collectors" placeholder="">
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="publication" class="custom-label-margin">Publication</label>
                                    <div class="row pl-3 pr-3" id="firstPublication">
                                        <input type="text" class="form-control col-md-5 form-control-lg" id="publication" placeholder="Publication">
                                        <input type="url" class="form-control col-md-5 form-control-lg ml-1" id="publicationLink" placeholder="Link to publication">
                                    </div>
                                    <button type="button" class="btn btn-link" id="morePublication"><i class="fas fa-plus-circle"></i> More publication</button>
                                    <div class="row pl-3 pr-3 mt-1" id="secondPublication">
                                        <input type="text" class="form-control form-control-lg col-md-5" id="publication" placeholder="Publication">
                                        <input type="url" class="form-control form-control-lg col-md-5 ml-1" id="publicationLink" placeholder="Link to publication">
                                    </div>
                                    <button type="button" class="btn btn-link" id="morePublication2"><i class="fas fa-plus-circle"></i> More publication</button>
                                    <div class="row pl-3 pr-3 mt-1" id="thirdPublication">
                                        <input type="text" class="form-control form-control-lg col-md-5" id="publication" placeholder="Publication">
                                        <input type="url" class="form-control form-control-lg col-md-5 ml-1" id="publicationLink" placeholder="Link to publication">
                                    </div>
                                </div>

                            </div>

                            <p class="d-inline">Po vložení záznam skontrolujeme, co nam nasledne trva do 14 dni. Nasledne vas zaznam zverejnime. </p>

                            <div class="d-flex align-items-center mt-4">
                                <a class="btn btn-link button-secondary-color-outline" href="new-record-step-1.php" role="button"><i class="fas fa-chevron-left"></i> Navrat na povinne udaje</a>
                                <button type="submit" class="btn button-secondary-color btn-lg ml-auto">Vytvorit zaznam </button>
                            </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>

</main>


<?php require 'about-us.html'; ?>
<?php require 'footer.html'; ?>
<?php require 'page-list.html'; ?>

<script type="text/javascript" src="public/node_modules/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="public/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="public/node_modules/toastr/toastr.js"></script>
<script type="text/javascript" src="public/node_modules/nette.ajax.js/nette.ajax.js"></script>
<script type="text/javascript" src="public/node_modules/nette-forms/src/assets/netteForms.js"></script>
<script type="text/javascript" src="public/node_modules/live-form-validation/live-form-validation.js"></script>
<script type="text/javascript" src="public/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="public/js/script.js"></script>

</body>
</html>
