/************************************/
/* DOM ready */
/************************************/
$(document).ready(function () {


    $.nette.init();

    $.nette.ext('loading', {
        before: function () {

        },
        complete: function () {

        }
    });

    $('[data-toggle="popover"]').popover();
    $('[data-toggle="tooltip"]').tooltip();

     /* open and close secondary row - search result with my DNA code */
    $(".close-row").hide();
    $(".open-row").click(function(){
        $(this).closest("tr").next(".close-row").toggle();
    });

    /* dokoncit spodne tlačidlo na zatvaranie */
    $(".close-row-button").click(function(){
        $(this).closest("tr.close-row").hide();
    });

    /* copy to clipboard */
    $("#record-detail-sequence-copy-button").click(function(){
        $("#record-detail-textarea").select();
        document.execCommand('copy');
        $("#record-detail-sequence-copy-message").show();
        setTimeout(
            function(){
                $("#record-detail-sequence-copy-message").hide();
            },
        3000);
    });

    /* show more comment */
    $(".record-detail-comments-hide").hide();
    $("#more-comment").click(function(){
        $(".record-detail-comments-hide").show();
        $("#more-comment").hide();
    });

    /* new record - more taxon */
    $("#secondaryTaxon").hide();
    $("#thirdTaxon").hide();
    $("#moreTaxon").click(function(){
        $("#secondaryTaxon").show();
        $("#thirdTaxon").show();
        $("#moreTaxon").hide();
    });

    /* more publication */
    $("#secondPublication").hide();
    $("#thirdPublication").hide();
    $("#morePublication2").hide();
    $("#morePublication").click(function(){
        $("#morePublication").hide();
        $("#secondPublication").show();
        $("#morePublication2").show();
    });
    $("#morePublication2").click(function(){
        $("#morePublication2").hide();
        $("#thirdPublication").show();
    });

    /* radio button chcecked and backgound */
    $(".custom-radio-button").click(function(){
        $(this).siblings().removeClass("bg-light");
        $(this).addClass("bg-light");
        $(this).children("input[type=radio]").prop( "checked", true );
    });

    $(".custom-radio-button > input[type=radio]:checked").parent().addClass("bg-light");


    /* Search and filter - specifick position - combination radio button and selectbox */
    $(".custom-specific-position").click(function(){
        $(".custom-specific-position").removeClass("bg-light");
        $(this).addClass("bg-light");
        $(this).closest('.custom-specific-position').find("input[type=radio]").prop( "checked", true );
    });

    $(".custom-specific-selectbox, .custom-specific-position button").click(function () {
        $(".custom-specific-position").removeClass("bg-light");
        $(this).closest('.custom-specific-position').toggleClass("bg-light");
        $(this).closest('.custom-specific-position').find("input[type=radio]").prop( "checked", true );
    });

    $(".custom-specific-position input[type=radio]:checked").parent().parent().parent().addClass("bg-light");

    /* td height */
    $(window).resize(function() {
        var intro_height = $(".complet-report table tbody tr:first td:first div").height();
        // alert(intro_height);
        $(".complet-report table tbody tr:first td div").height(intro_height);
    });

    /* search result - select row */
    $(".complet-report table td .left-box label input").click(function(){
        $(this).closest(".row-with-checkbox").children().toggleClass("manual-selected-row");
        $(this).closest(".row-with-checkbox").siblings("td").children().toggleClass("manual-selected-row");
        $(this).closest("tr").next().find(".close-row-box").toggleClass("manual-selected-row");
    });
    $(".complet-report table td .left-box label input:checked").closest(".row-with-checkbox").siblings("td").children().addClass("manual-selected-row");
    $(".complet-report table td .left-box label input:checked").closest(".row-with-checkbox").children().addClass("manual-selected-row");
    $(".complet-report table td .left-box label input:checked").closest("tr").next().find(".close-row-box").addClass("manual-selected-row");

    $(".unsellect-all").click(function(){
        //$(".complet-report table tbody td .left-box input").prop('checked', this.checked);
        $(".complet-report table tbody td .left-box input").prop('checked', false);
        $(".complet-report table tbody td div").removeClass("manual-selected-row");

        $("#selected-export").prop('disabled', true);
        $("#export-box").addClass('d-none');
        $("#selected-number").text("0");
        $("#selected-number-bottom").text("0");
    });


    /* export selected row */
    var numberOfChecked = $('.complet-report input:checkbox:checked').length;
    var totalCheckboxes = $('.complet-report input:checkbox').length;
    var numberNotChecked = totalCheckboxes - numberOfChecked;

    $("#selected-number").text(numberOfChecked);
    $("#selected-number-bottom").text(numberOfChecked);

    if ($("#selected-number").text() != ('0') ) {
        $("#selected-export").removeAttr('disabled');
    }
    if ($("#selected-number").text() != ('0') ) {
        $("#export-box").removeClass('d-none');
    }


    $(".complet-report table td .left-box label input").click(function(){
        var numberOfChecked = $('.complet-report input:checkbox:checked').length;
        var totalCheckboxes = $('.complet-report input:checkbox').length;
        var numberNotChecked = totalCheckboxes - numberOfChecked;

        $("#selected-number").text(numberOfChecked);
        $("#selected-number-bottom").text(numberOfChecked);

        if ($("#selected-number").text() != ('0') ) {
            $("#selected-export").removeAttr('disabled');
            $("#export-box").removeClass('d-none');
        }
        else {
            $("#selected-export").prop('disabled', true);
            $("#export-box").addClass('d-none');
        }
    });






    /* pomocne na vypis elementu na ktory sa klika
    $( "body" ).click(function( event ) {
        $( "#log" ).html( "c: " + event.target.nodeName );
    });
    */


});
