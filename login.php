<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Language" content="cs">
    <meta name="created" content="Liquid Design s.r.o.">
    <link rel="shortcut icon" href="/jjtmpublic/favicon.ico">




    <!-- Font Awesome -->
    <link rel="stylesheet" href="public/node_modules/normalize.css/normalize.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="public/node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="public/node_modules/@fortawesome/fontawesome-free/css/all.css" rel="stylesheet" type="text/css">
    <link href="public/css/lightbox.min.css" rel="stylesheet" type="text/css">
    <link href="public/css/base.css" rel="stylesheet" type="text/css">
    <link href="public/css/front.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">


    <!--[if its IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- autorefresh
    <meta http-equiv="refresh" content="30" /> -->

</head>
<body>

<?php require 'menu.html'; ?>

<main class="bg-light pb-5">
    <div class="container">
        <div class="row">
            <nav aria-label="breadcrumb" class="col-12">
                <ol class="breadcrumb bg-transparent pl-0">
                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Registration </li>
                </ol>
            </nav>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="offset-md-2 col-md-8">
                    <h1 class="d-inline-block mr-1 mr-md-3">Login</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="bg-color-form pt-4 pb-4">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="offset-md-2 col-md-8">
                        <form>
                            <div class="bg-white border rounded p-3 mb-2">
                                <div class="form-group">
                                    <label for="primaryTaxon" class="custom-label-margin">* E-mail</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="email" class="form-control form-control-lg" id="collectors" placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="primaryTaxon" class="custom-label-margin">* Password</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="password" class="form-control form-control-lg" id="collectors" placeholder="">
                                        </div>
                                    </div>
                                </div>



                            </div>

                            <div class="d-flex flex-column align-items-center mt-4">
                                <a class="btn button-secondary-color btn-lg" href="search-result-2.php" role="button">Login </a>
                                <span class="mt-5"><a href="">Forgot your password?</a></span>
                                <span class="">Need a new account? <a href="registration.php">Sign Up</a></span>
                            </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>

</main>

<?php require 'about-us.html'; ?>
<?php require 'footer.html'; ?>
<?php require 'page-list.html'; ?>

<div class="container d-fles"></div>

<script type="text/javascript" src="public/node_modules/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="public/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="public/node_modules/toastr/toastr.js"></script>
<script type="text/javascript" src="public/node_modules/nette.ajax.js/nette.ajax.js"></script>
<script type="text/javascript" src="public/node_modules/nette-forms/src/assets/netteForms.js"></script>
<script type="text/javascript" src="public/node_modules/live-form-validation/live-form-validation.js"></script>
<script type="text/javascript" src="public/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="public/js/script.js"></script>

</body>
</html>
