<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Language" content="cs">
    <meta name="created" content="Liquid Design s.r.o.">
    <link rel="shortcut icon" href="/jjtmpublic/favicon.ico">




    <!-- Font Awesome -->
    <link rel="stylesheet" href="public/node_modules/normalize.css/normalize.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="public/node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="public/node_modules/@fortawesome/fontawesome-free/css/all.css" rel="stylesheet" type="text/css">
    <link href="public/css/lightbox.min.css" rel="stylesheet" type="text/css">
    <link href="public/css/base.css" rel="stylesheet" type="text/css">
    <link href="public/css/front.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">


    <!--[if its IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>
<body>

<?php require 'menu.html'; ?>


<main class="bg-light pb-5">
    <div class="container">
        <div class="row">
            <nav aria-label="breadcrumb" class="col-12">
                <ol class="breadcrumb bg-transparent pl-0">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="search-result-2.php">Search result</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Record detail</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-12">
                <a href="search-result-2.php"><button type="button" class="btn btn-outline-secondary""><i class="fas fa-long-arrow-alt-left"></i> Back to search result</button></a>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <h1 class="d-inline-block mr-1 mr-md-3">Carassius auratus #21412</h1><a href="" class="align-middle d-inline-block mb-3"><button type="button" class="btn button-secondary-color"><i class="fas fa-download"></i> Export record</button></a>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <ul class="nav nav-tabs record-detail-tablist mt-3" id="myTab" role="tablist">
                    <li class="nav-item pl-0 pr-0 text-center col-md-6">
                        <a class="nav-link active d-flex justify-content-center align-items-center font-weight-bold" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Basic info</a>
                    </li>
                    <li class="nav-item pl-0 pr-0 text-center col-md-6">
                        <a class="nav-link d-flex justify-content-center align-items-center font-weight-bold" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">S7iCAPS Method</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-12 pb-4">
                <div class="tab-content bg-white mt-0 pt-4 pb-4 border border-top-0 rounded bg-shadow" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                        <div class="d-flex">
                            <div class="col-md-6">
                                <ul class="list-group list-group-flush border bg-white rounded">
                                    <li class="list-group-item border-top-0 rounded-top"><strong>Family:</strong> Cyprinidae</li>
                                    <li class="list-group-item"><strong>Genus:</strong> Alburnoides</li>
                                    <li class="list-group-item"><strong>Paired Second Taxon:</strong> secondary taxon</li>
                                    <li class="list-group-item"><strong>Group Third Taxon (Polypoid):</strong> </li>
                                    <li class="list-group-item"><strong>Nuclear Diagnostics:</strong> Indel</li>
                                    <li class="list-group-item"><strong>Type specimen:</strong> Hybrid</li>
                                    <li class="list-group-item rounded-bottom">
                                        <div class="form-group d-flex flex-wrap">
                                            <label for="record-detail-textarea" class="flex-grow-1 mb-0">Sequence in FASTA format:</label>
                                            <span class="mr-2 text-success" id="record-detail-sequence-copy-message"><i class="fas fa-check"></i> copied</span>
                                            <button type="button" class="btn btn-secondary btn-sm" id="record-detail-sequence-copy-button"><i class="far fa-copy"></i> Copy</button>
                                            <textarea class="form-control mt-1" id="record-detail-textarea" rows="4" style="border-color: rgb(223, 225, 230);background-color: rgb(250, 251, 252);border-width: 2px;">>Vimba_vimba_0045<br>CGAAAATGCCTCTATTAAGTAAAGTACTATATCTCGAAGACTTATATTTGTGATAGAATTAAAGACAATC</textarea>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <img src="public/img/mapa.png" alt="mapa" class="img-fluid"><img src="public/img/mapa.png" alt="mapa" class="img-fluid">
                            </div>
                        </div>


                        <div class="mt-3 col-12">
                            <h2>Geography - 10 Localities</h2>
                            <span><i class="far fa-flag"></i> 5x Germany, 2x Czech, 1x Slovakia <i class="fas fa-map-marker-alt ml-3"></i> 5x Rieka, 3x Chov, 2x zahranicie</span>
                        </div>

                        <div class="d-flex flex-wrap">

                            <div class="col-md-6 locality-detail mb-4">
                                <div class="bg-white border rounded">
                                    <h3 class="border-bottom mt-0 col-12"><!-- Main River Basin: --> Danube, <!-- Country: --> <img src="public/img/czech-republic.png" alt=""> Czech republik </h3>

                                    <div class="d-flex flex-column flex-xl-row">
                                        <ul class="list-unstyled col-xl-6">
                                            <li><strong>Resource Type:</strong> River</li>
                                            <li><strong>See Drainage:</strong> Black Sea</li>
                                            <li><strong>Latitude:</strong> 48.5320</li>
                                            <li><strong>Longitude:</strong> 15.3856</li>
                                            <li><strong>Fishing Area:</strong> Europe</li>
                                            <li class="d-flex"><strong>Sex:</strong> <i class="fas fa-mars text-primary fa-2x ml-2 mr-1"></i> Male</li>
                                            <li><strong>Collection Date:</strong> 13.11.2017</li>
                                            <li><strong>Collectors:</strong> Matej Chyla</li>
                                            <li><strong>Deposited in:</strong> Ustav XYZ</li>
                                        </ul>
                                        <div class="col-xl-6">
                                            <a href="https://unsplash.it/600.jpg?image=251" data-lightbox="roadtrip" data-title="popisok obrazku">
                                                <img src="https://unsplash.it/600.jpg?image=251" alt="hlavny" class="img-fluid rounded">
                                            </a>
                                            <div class="row p-3 pr-3">
                                                <a href="https://unsplash.it/600.jpg?image=253" data-lightbox="roadtrip" class="col-4 pl-0 pr-2"><img src="https://unsplash.it/600.jpg?image=253" alt="maly" class="img-fluid rounded"></a>
                                                <a href="https://unsplash.it/600.jpg?image=254" data-lightbox="roadtrip" class="col-4 pl-1 pr-1"><img src="https://unsplash.it/600.jpg?image=254" alt="maly" class="img-fluid rounded"></a>
                                                <a href="https://unsplash.it/600.jpg?image=252" data-lightbox="roadtrip" class="col-4 pl-2 pr-0"><img src="https://unsplash.it/600.jpg?image=252" alt="maly" class="img-fluid rounded"></a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-6 locality-detail mb-4">
                                <div class="bg-white border rounded">
                                    <h3 class="border-bottom mt-0 col-12"><!-- Company name --> Liquid Fish s.r.o., <!-- Pri type vlastny chov bude vzdy Cesko --> <img src="public/img/czech-republic.png" alt=""> Czech republik </h3>

                                    <div class="d-flex flex-column flex-xl-row">
                                        <ul class="list-unstyled col-xl-6">
                                            <li><strong>Resource Type:</strong> Self-sustaining stock</li>
                                            <li><strong>Pond/Hatchery/Broodstock:</strong> Zlab Brno 12</li>
                                            <li><strong>Latitude:</strong> 48.5320</li>
                                            <li><strong>Longitude:</strong> 15.3856</li>
                                            <li class="d-flex"><strong>Sex:</strong> <i class="fas fa-mars text-primary fa-2x ml-2 mr-1"></i> Male</li>
                                            <li><strong>Collection Date:</strong> 13.11.2017</li>
                                            <li><strong>Collectors:</strong> Matej Chyla</li>
                                        </ul>
                                        <div class="col-xl-6">
                                            <a href="https://unsplash.it/600.jpg?image=251" data-lightbox="roadtrip2" data-title="popisok obrazku">
                                                <img src="https://unsplash.it/600.jpg?image=251" alt="hlavny" class="img-fluid rounded">
                                            </a>
                                            <div class="row p-3 pr-3">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-6 locality-detail mb-4">
                                <div class="bg-white border rounded">
                                    <h3 class="border-bottom mt-0 col-12"><!-- Locality (City/Region) --> Berlin, <!-- Country --> <img src="public/img/germany.png" alt=""> Germany</h3>

                                    <div class="d-flex flex-column flex-xl-row">
                                        <ul class="list-unstyled col-xl-6">
                                            <li><strong>Resource Type:</strong> Import trade</li>
                                            <li><strong>Company:</strong> Berlin Fish Market </li>
                                            <li><strong>Business name:</strong> BFISH Market s.r.o.</li>
                                            <li class="d-flex"><strong>Sex:</strong> <i class="fas fa-venus text-danger fa-2x ml-2 mr-1"></i> Female</li>
                                            <li><strong>Collection Date:</strong> 13.11.2017</li>
                                            <li><strong>Collectors:</strong> Matej Chyla</li>
                                            <li><strong>Deposited in:</strong> Ustav XYZ</li>
                                        </ul>
                                        <div class="col-xl-6">
                                            <a href="https://unsplash.it/600.jpg?image=251" data-lightbox="roadtrip3" data-title="popisok obrazku">
                                                <img src="https://unsplash.it/600.jpg?image=251" alt="hlavny" class="img-fluid rounded">
                                            </a>
                                            <div class="row p-3 pr-3">
                                                <a href="https://unsplash.it/600.jpg?image=252" data-lightbox="roadtrip3" class="col-4 pl-2 pr-0"><img src="https://unsplash.it/600.jpg?image=252" alt="maly" class="img-fluid rounded"></a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6 locality-detail mb-4">
                                <div class="bg-white border rounded">
                                    <h3 class="border-bottom mt-0 col-12"><!-- Main River Basin: --> Danube, <!-- Country: --> <img src="public/img/czech-republic.png" alt=""> Czech republik </h3>

                                    <div class="d-flex flex-column flex-xl-row">
                                        <ul class="list-unstyled col-xl-6">
                                            <li><strong>Resource Type:</strong> River</li>
                                            <li><strong>See Drainage:</strong> Black Sea</li>
                                            <li><strong>Latitude:</strong> 48.5320</li>
                                            <li><strong>Longitude:</strong> 15.3856</li>
                                            <li><strong>Fishing Area:</strong> Europe</li>
                                            <li class="d-flex"><strong>Sex:</strong> <i class="fas fa-mars text-primary fa-2x ml-2 mr-1"></i> Male</li>
                                            <li><strong>Collection Date:</strong> 13.11.2017</li>
                                            <li><strong>Collectors:</strong> Matej Chyla</li>
                                            <li><strong>Deposited in:</strong> Ustav XYZ</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="rounded bg-color-primary pb-3 text-center background-gradient">
                                <h2 class="mb-0 text-white">Nova lokalita</h2>
                                <p class="mt-0 text-white big-text">Našli ste vzorku aj inde? Nahrajte ju do nášej databázy, zaberie to len pár sekúnd a veľmi nám tým pomôžete</p>
                                <div class="text-center">
                                    <a href="new-location.php"><button type="button" class="btn button-primary-color">Pridať lokalitu ku sequencií</button></a>
                                </div>
                            </div>
                        </div>


                        <div class="d-flex">
                            <div class="col-md-6  mt-3">
                                <h2>More information</h2>
                                <ul class="list-group list-group-flush border rounded bg-white">
                                    <li class="list-group-item border-top-0 rounded-top"><strong>Sex specific marker:</strong> </li>
                                    <li class="list-group-item"><strong>GenBank Accession No:</strong> </li>
                                    <li class="list-group-item"><strong>BoLD Sample ID:</strong> IFCZE1429</li>
                                    <li class="list-group-item rounded-bottom"><strong>Publication:</strong>
                                        <span> <a href="#publication-link"> Publication name</a> </span>,
                                        <span>Publication name </span>,
                                        <span>Publication name </span>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-md-6  mt-3">
                                <h2>Comments (4)</h2>
                                <div class="rounded border bg-white">

                                    <!--
                                    <div class="p-3">
                                        <p><i class="fas fa-comments"></i> No comments</p>
                                    </div>
                                    -->

                                    <div class="p-3">
                                        <div class="border-bottom mb-3">
                                            <span><strong>Matej Chyla, 9.12.2018</strong></span>
                                            <p class="mt-0 mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                                        </div>
                                        <div class="border-bottom mb-3">
                                            <span><strong>Matej Chyla, 10.12.2018</strong></span>
                                            <p class="mt-0 mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                                        </div>
                                        <div class="border-bottom mb-3 record-detail-comments-hide">
                                            <span><strong>Matej Chyla, 12.12.2018</strong></span>
                                            <p class="mt-0 mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                                        </div>
                                        <div class="border-bottom mb-3 record-detail-comments-hide">
                                            <span><strong>Matej Chyla, 14.12.2018</strong></span>
                                            <p class="mt-0 mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                                        </div>

                                        <span class="d-inline-block col-12 text-center"><a href="#more-comment" id="more-comment">More comments (2)</a></span>
                                    </div>


                                    <div class="bg-light p-3 rounded-bottom">
                                        <form>
                                            <div class="form-group">
                                                <label for="exampleFormControlTextarea1">New comment</label>
                                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="1" placeholder="Comment"></textarea>
                                            </div>
                                            <button type="button" class="btn btn-secondary btn-sm">Send comment</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="d-flex flex-wrap mb-3">
                            <h3 class="col-12 mb-0">Identification system 1: C. auratus x C. carpio</h3>
                            <div class="col-md-6">
                                <ul class="list-group list-group-flush border bg-white rounded">
                                    <li class="list-group-item border-top-0 rounded-top"><strong>JGM region (bp):</strong> 579 and 585</li>
                                    <li class="list-group-item"><strong>REAssay 1:</strong> Dral</li>
                                    <li class="list-group-item"><strong>Restriction motif:</strong> TTTAAA</li>
                                    <li class="list-group-item"><strong>Fragment Length (bp):</strong> 520, 59 (C.A); 450, 135 (CC)</li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <img src="public/img/ilustracne.png" alt="" class="img-fluid">
                                <span>virtual gel</span>
                            </div>
                        </div>

                        <div class="d-flex flex-wrap mb-3">
                            <h3 class="col-12 mb-0">Identification system 2:  C. auratus x C. carassius</h3>
                            <div class="col-md-6">
                                <ul class="list-group list-group-flush border bg-white rounded">
                                    <li class="list-group-item border-top-0 rounded-top"><strong>JGM region (bp):</strong> 579 and 585</li>
                                    <li class="list-group-item"><strong>REAssay 1:</strong> Dral</li>
                                    <li class="list-group-item"><strong>Restriction motif:</strong> TTTAAA</li>
                                    <li class="list-group-item"><strong>Fragment Length (bp):</strong> 520, 59 (C.A); 450, 135 (CC)</li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <img src="public/img/ilustracne.png" alt="" class="img-fluid">
                                <span>virtual gel</span>
                            </div>
                        </div>

                        <div class="d-flex flex-wrap mb-3">
                            <h3 class="col-12 mb-0">Identification system 3:  C. auratus x C. langsdorfii</h3>
                            <div class="col-md-6">
                                <ul class="list-group list-group-flush border bg-white rounded">
                                    <li class="list-group-item border-top-0 rounded-top"><strong>JGM region (bp):</strong> 579 and 585</li>
                                    <li class="list-group-item"><strong>REAssay 1:</strong> Dral</li>
                                    <li class="list-group-item"><strong>Restriction motif:</strong> TTTAAA</li>
                                    <li class="list-group-item"><strong>Fragment Length (bp):</strong> 520, 59 (C.A); 450, 135 (CC)</li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <img src="public/img/ilustracne.png" alt="" class="img-fluid">
                                <span>virtual gel</span>
                            </div>
                        </div>

                        <div class="col-12 mt-5">
                            <div class="d-flex flex-column rounded bg-color-primary text-center background-gradient">
                                <h2 class="mb-0 text-white">Zakupenie diagnostickeho kitu</h2>

                                <p class="offset-md-1 col-md-10 mt-0 text-white big-text">Máte záujem a rýchle a lacné rozpoznanie - zakupte si od nás KIT na rozpoznanie - ten obsahuje:</p>
                                <ul class="list-unstyled d-flex flex-wrap justify-content-center offset-md-1 col-md-10 list-unstyled">
                                    <li class=""><h4><i class="fas fa-check "></i> Potrebnu chemiu</h4></li>
                                    <li class="ml-4 mr-4"><h4><i class="fas fa-check "></i> Popis postupu</h4></li>
                                    <li class=""><h4><i class="fas fa-check "></i> Diagnosticky motiv</h4></li>
                                </ul>
                                <p class="offset-md-1 col-md-10 mt-0 text-white big-text">V prípade záujmu nás <a href="">môžete kontaktovat</a></p>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>
</main>

<?php require 'about-us.html'; ?>
<?php require 'footer.html'; ?>
<?php require 'page-list.html'; ?>

<script type="text/javascript" src="public/node_modules/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="public/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="public/node_modules/toastr/toastr.js"></script>
<script type="text/javascript" src="public/node_modules/nette.ajax.js/nette.ajax.js"></script>
<script type="text/javascript" src="public/node_modules/nette-forms/src/assets/netteForms.js"></script>
<script type="text/javascript" src="public/node_modules/live-form-validation/live-form-validation.js"></script>
<script type="text/javascript" src="public/js/lightbox.min.js"></script>
<script type="text/javascript" src="public/js/script.js"></script>

</body>
</html>
