<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Language" content="cs">
    <meta name="created" content="Liquid Design s.r.o.">
    <link rel="shortcut icon" href="/jjtmpublic/favicon.ico">




    <!-- Font Awesome -->
    <link rel="stylesheet" href="public/node_modules/normalize.css/normalize.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="public/node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="public/node_modules/@fortawesome/fontawesome-free/css/all.css" rel="stylesheet" type="text/css">
    <link href="public/css/lightbox.min.css" rel="stylesheet" type="text/css">
    <link href="public/css/base.css" rel="stylesheet" type="text/css">
    <link href="public/css/front.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">


    <!--[if its IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- autorefresh
    <meta http-equiv="refresh" content="30" /> -->

</head>
<body>

<?php require 'menu.html'; ?>

<main class="bg-light">
    <div class="hp-intro">
        <div class="container">
            <div class="row">
                <div class="col-12 mt-3">
                    <h1 class="">Fish Identification Tools for Biodiversity and Aquaculture</h1>
                    <h2 class="mb-5">S7iFish database and nuclear DNA-based species determination</h2>
                </div>
            </div>
            <div class="row pb-3 pb-md-5">
                <div class="col-12">
                    <a href="registration.php"><button type="button" class="btn btn-lg mr-2 button-primary-color">Create account</button></a>
                    <a href="application-info.php" class="mt-2 d-inline-block"><button type="button" class="btn btn-outline-light btn-lg">More information</button></a>
                </div>
            </div>
        </div>
    </div>


    <div class="pb-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?php require 'search-filter.html'; ?>
                </div>
            </div>

            <div class="row">
                <div class="col-12 ">
                    <img src="public/img/mapa.png" alt="mapa" class="rounded mx-auto d-block img-fluid">
                </div>
            </div>
        </div>
    </div>
</main>


<?php require 'about-us.html'; ?>
<?php require 'footer.html'; ?>
<?php require 'page-list.html'; ?>

<script type="text/javascript" src="public/node_modules/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="public/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="public/node_modules/toastr/toastr.js"></script>
<script type="text/javascript" src="public/node_modules/nette.ajax.js/nette.ajax.js"></script>
<script type="text/javascript" src="public/node_modules/nette-forms/src/assets/netteForms.js"></script>
<script type="text/javascript" src="public/node_modules/live-form-validation/live-form-validation.js"></script>
<script type="text/javascript" src="public/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="public/js/script.js"></script>



</body>
</html>
