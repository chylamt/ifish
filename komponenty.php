
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Language" content="cs">
    <meta name="created" content="Liquid Design s.r.o.">
    <link rel="shortcut icon" href="/jjtmpublic/favicon.ico">




    <!-- Font Awesome -->
    <link rel="stylesheet" href="public/node_modules/normalize.css/normalize.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="public/node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="public/node_modules/@fortawesome/fontawesome-free/css/all.css" rel="stylesheet" type="text/css">
    <link href="public/css/lightbox.min.css" rel="stylesheet" type="text/css">
    <link href="public/css/base.css" rel="stylesheet" type="text/css">
    <link href="public/css/front.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">


    <!--[if its IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- autorefresh -->
    <meta http-equiv="refresh" content="30" />

</head>
<body>

<?php require 'menu.html'; ?>

<main>
    <div class="container">

        <div class="row mt-5">
            <div class="col-12">
                <form class="bg-light p-3">

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">
                                <i data-toggle="tooltip" data-placement="top" title="Taxon">Taxon,</i>
                                <i data-toggle="tooltip" data-placement="top" title="Genus - zadajte nazov">Genus,</i>
                                <i data-toggle="tooltip" data-placement="top" title="Voucher ID je identifikacne 5-miestne cislo, napr. 15421">Voucher ID</i>
                            </label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Carassius auratus">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleFormControlSelect1">Example select</label>
                            <select class="form-control selectpicker" id="exampleFormControlSelect1" data-live-search="true" title="Vyberte jednu alebo viac krajín" multiple>
                                <option data-tokens="česko česká cesko">Czech republic</option>
                                <option data-tokens="Slovensko slovensko">Slovakia</option>
                                <option data-tokens="Nemecko">Germany</option>
                            </select>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </div>
    </div>
</main>

<div class="container d-fles"></div>

<script type="text/javascript" src="public/node_modules/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="public/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="public/node_modules/toastr/toastr.js"></script>
<script type="text/javascript" src="public/node_modules/nette.ajax.js/nette.ajax.js"></script>
<script type="text/javascript" src="public/node_modules/nette-forms/src/assets/netteForms.js"></script>
<script type="text/javascript" src="public/node_modules/live-form-validation/live-form-validation.js"></script>
<script type="text/javascript" src="public/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="public/js/script.js"></script>



</body>
</html>
