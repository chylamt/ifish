<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Language" content="cs">
    <meta name="created" content="Liquid Design s.r.o.">
    <link rel="shortcut icon" href="/jjtmpublic/favicon.ico">




    <!-- Font Awesome -->
    <link rel="stylesheet" href="public/node_modules/normalize.css/normalize.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="public/node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="public/node_modules/@fortawesome/fontawesome-free/css/all.css" rel="stylesheet" type="text/css">
    <link href="public/css/lightbox.min.css" rel="stylesheet" type="text/css">
    <link href="public/css/base.css" rel="stylesheet" type="text/css">
    <link href="public/css/front.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">


    <!--[if its IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- autorefresh
    <meta http-equiv="refresh" content="30" /> -->

</head>
<body>

<?php require 'menu-after-login.html'; ?>

<main class="bg-light pb-5">
    <div class="container">
        <div class="row">
            <nav aria-label="breadcrumb" class="col-12">
                <ol class="breadcrumb bg-transparent pl-0">
                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Account </li>
                </ol>
            </nav>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="offset-md-2 col-md-8">
                    <h1 class="d-inline-block mr-1 mr-md-3">User account</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="bg-color-form pt-4 pb-4">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="offset-md-2 col-md-8">
                        <form>
                            <div class="bg-white border rounded p-3 mb-2">
                                <ul>
                                    <li><strong>Meno a prjmeni:</strong> Matej Chyla</li>
                                    <li><strong>E-mail:</strong>matej@lqd.cz</li>
                                    <li><strong>Heslo:</strong> *****</li>
                                    <li><strong>Organizace:</strong> Liquid Design</li>
                                    <li><strong>Telefon</strong> - </li>
                                </ul>
                            </div>

                            <div class="d-flex flex-column align-items-center mt-4">
                                <a class="btn button-secondary-color btn-lg" href="" role="button">Upraviť údaje </a>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!--
    <div class="bg-color-form pt-4 pb-4">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="offset-md-2 col-md-8">
                        <form>
                            <div class="bg-white border rounded p-3 mb-2">
                                <div class="form-group">
                                    <label for="primaryTaxon" class="custom-label-margin">* Meno a prijmeni</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control form-control-lg" id="collectors" placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="primaryTaxon" class="custom-label-margin">* E-mail</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="email" class="form-control form-control-lg" id="collectors" placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="primaryTaxon" class="custom-label-margin">* Heslo</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="password" class="form-control form-control-lg" id="collectors" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="primaryTaxon" class="custom-label-margin">* Heslo znovu</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="password" class="form-control form-control-lg" id="collectors" placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="primaryTaxon" class="custom-label-margin">Organizace</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control form-control-lg" id="collectors" placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="primaryTaxon" class="custom-label-margin">Telefon (nebude verejný, môže sa hodiť pri upresnení vami vkladaných záznamov)</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="tel" class="form-control form-control-lg" id="collectors" placeholder="">
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="d-flex flex-column align-items-center mt-4">
                                <a class="btn button-secondary-color btn-lg" href="" role="button">Ulozit udaje </a>
                            </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>
    -->

    <div class="bg-color-form pb-3">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="mb-0">Your reccord: </h2>
                    <p class="mt-0">V pripade ze by ste chceli zaznam aktualizovat alebo dopnit <a href="">nas kontaktujte</a></p>
                </div>
            </div>

            <div class="row complet-report">
                <div class="col-12 mt-3">
                    <div class="table-responsive-xl">
                        <table class="table">
                            <thead class="">
                            <tr class="bg-shadow">
                                <th scope="col">
                                    <div class="left-box pl-2 d-flex align-items-center bg-color-primary">
                                        <button class="unsellect-all btn button-primary-color-outline btn-sm"><i class="fas fa-times ml-1 mr-1"></i>Unsellect All</button>
                                    </div>
                                </th>
                                <th scope="col">
                                    <div class="pl-2 d-flex align-items-stretch bg-color-primary">
                                        <span class="d-flex align-items-center">Country</span>
                                    </div>
                                </th>
                                <th scope="col">
                                    <div class="pl-2 d-flex align-items-stretch bg-color-primary">
                                        <span class="d-flex align-items-center">Location</span>
                                    </div>
                                </th>
                                <th scope="col">
                                    <div class="pl-2 d-flex align-items-stretch bg-color-primary">
                                        <span class="d-flex align-items-center">Created</span>
                                    </div>
                                </th>
                                <th scope="col">
                                    <div class="pl-2 d-flex align-items-stretch bg-color-primary">
                                        <span class="d-flex align-items-center">ID</span>
                                    </div>
                                </th>
                                <th scope="col">
                                    <div class="pl-2 d-flex align-items-stretch bg-color-primary">
                                        <span class="d-flex align-items-center">Foto</span>
                                    </div>
                                </th>
                                <th scope="col">
                                    <div class="pl-2 d-flex align-items-stretch bg-color-primary">
                                        <span class="d-flex align-items-center">Cover</span>
                                    </div>
                                </th>
                                <th scope="col">
                                    <div class="pl-2 d-flex align-items-stretch bg-color-primary">
                                        <span class="d-flex align-items-center">Match</span>

                                    </div>
                                </th>
                                <th scope="col">
                                    <div class="right-box pl-2 d-flex align-items-center justify-content-end bg-color-primary">
                                        <button type="button" class="btn button-primary-color-outline btn-sm mr-2"><i class="fas fa-download"></i> All (15)</button>
                                        <button type="button" class="btn button-primary-color btn-sm mr-2" disabled id="selected-export"><i class="fas fa-download"></i> Sellected (<span id="selected-number">0</span>)</button>
                                    </div>
                                </th>
                            </tr>
                            </thead>

                            <tbody>
                            <tr>
                                <td scope="row" class="row-with-checkbox">
                                    <div class="left-box rounded-left mt-2 pl-2 bg-white d-flex align-items-stretch">
                                        <label class="d-flex align-items-stretch">
                                            <input type="checkbox" name="check" class="">
                                            <span class="label-text align-self-center pt-1 pb-1">Phoxinus phoxinus lineage</span>
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="center-box mt-2 pl-2 bg-white d-flex align-items-stretch">
                                        <span class="align-self-center"><i class="far fa-flag"></i> 1x</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                        <span class="align-self-center"><i class="fas fa-map-marker-alt"></i> 3x</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                        <span class="align-self-center">5.8.2017</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                        <span class="align-self-center">#12312</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                        <span class="align-self-center"><i class="far fa-image"></i> 2x</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                        <span class="align-self-center"> - </span>
                                    </div>
                                </td>
                                <td>
                                    <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                        <span class="align-self-center"> - </span>
                                    </div>
                                </td>
                                <td class="">
                                    <div class="right-box rounded-right mt-2 pl-2 pr-2 bg-white d-flex align-items-center justify-content-end">
                                        <button type="button" class="btn button-secondary-color-outline ml-1 mr-1"><i class="fas fa-download"></i></button>
                                        <a href="record-detail.php"><button type="button" class="btn button-secondary-color">Detail <i class="fas fa-chevron-right"></i></button></a>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td scope="row" class="row-with-checkbox">
                                    <div class="left-box rounded-left mt-2 pl-2 bg-white d-flex align-items-stretch">
                                        <label class="d-flex align-items-center">
                                            <input type="checkbox" name="check" class="">
                                            <span class="label-text">Phoxinus phoxinus lineage</span>
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="center-box mt-2 pl-2 bg-white d-flex align-items-stretch">
                                        <span class="align-self-center"><i class="far fa-flag"></i> 1x</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                        <span class="align-self-center"><i class="fas fa-map-marker-alt"></i> 3x</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                        <span class="align-self-center">5.8.2017</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                        <span class="align-self-center">#12312</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                        <span class="align-self-center"><i class="far fa-image"></i> 2x</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                        <span class="align-self-center"> - </span>
                                    </div>
                                </td>
                                <td>
                                    <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                        <span class="align-self-center"> - </span>
                                    </div>
                                </td>
                                <td class="">
                                    <div class="right-box rounded-right mt-2 pl-2 pr-2 bg-white d-flex align-items-center justify-content-end">
                                        <button type="button" class="btn button-secondary-color-outline ml-1 mr-1"><i class="fas fa-download"></i></button>
                                        <a href="record-detail.php"><button type="button" class="btn button-secondary-color">Detail <i class="fas fa-chevron-right"></i></button></a>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td scope="row" class="row-with-checkbox">
                                    <div class="left-box rounded-left mt-2 pl-2 bg-white d-flex align-items-stretch">
                                        <label class="d-flex align-items-center">
                                            <input type="checkbox" name="check" class="">
                                            <span class="label-text">Phoxinus lineage</span>
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="center-box mt-2 pl-2 bg-white d-flex align-items-stretch">
                                        <span class="align-self-center"><i class="far fa-flag"></i> 1x</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                        <span class="align-self-center"><i class="fas fa-map-marker-alt"></i> 3x</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                        <span class="align-self-center">5.8.2017</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                        <span class="align-self-center">#12312</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                        <span class="align-self-center"><i class="far fa-image"></i> 2x</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                        <span class="align-self-center"> - </span>
                                    </div>
                                </td>
                                <td>
                                    <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                        <span class="align-self-center"> - </span>
                                    </div>
                                </td>
                                <td class="">
                                    <div class="right-box rounded-right mt-2 pl-2 pr-2 bg-white d-flex align-items-center justify-content-end">
                                        <button type="button" class="btn button-secondary-color-outline ml-1 mr-1"><i class="fas fa-download"></i></button>
                                        <a href="record-detail.php"><button type="button" class="btn button-secondary-color">Detail <i class="fas fa-chevron-right"></i></button></a>
                                    </div>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div>


</main>


<?php require 'about-us.html'; ?>
<?php require 'footer.html'; ?>
<?php require 'page-list.html'; ?>

<div class="select-table-row d-none" id="export-box">
    <div class="container">
        <div class="row pt-3 pb-3">
            <div class="col-12 d-flex align-items-center">
                <div class="label-text pl-2">Vybraných záznamov: <span id="selected-number-bottom">0</span></div>

                <div class="ml-auto pr-2">
                    <button type="button" class="btn btn-outline-light unsellect-all"><i class="fas fa-times ml-1 mr-1"></i>Unsellect all record</button></button>
                    <button type="button" class="btn button-secondary-color"><i class="fas fa-download"></i> Export selected record</button>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container d-fles"></div>

<script type="text/javascript" src="public/node_modules/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="public/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="public/node_modules/toastr/toastr.js"></script>
<script type="text/javascript" src="public/node_modules/nette.ajax.js/nette.ajax.js"></script>
<script type="text/javascript" src="public/node_modules/nette-forms/src/assets/netteForms.js"></script>
<script type="text/javascript" src="public/node_modules/live-form-validation/live-form-validation.js"></script>
<script type="text/javascript" src="public/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="public/js/script.js"></script>

</body>
</html>
