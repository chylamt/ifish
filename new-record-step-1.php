<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Language" content="cs">
    <meta name="created" content="Liquid Design s.r.o.">
    <link rel="shortcut icon" href="/jjtmpublic/favicon.ico">




    <!-- Font Awesome -->
    <link rel="stylesheet" href="public/node_modules/normalize.css/normalize.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="public/node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="public/node_modules/@fortawesome/fontawesome-free/css/all.css" rel="stylesheet" type="text/css">
    <link href="public/css/lightbox.min.css" rel="stylesheet" type="text/css">
    <link href="public/css/base.css" rel="stylesheet" type="text/css">
    <link href="public/css/front.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">


    <!--[if its IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- autorefresh
    <meta http-equiv="refresh" content="30" /> -->

</head>
<body>

<?php require 'menu.html'; ?>

<main class="bg-light pb-5">
    <div class="container">
        <div class="row">
            <nav aria-label="breadcrumb" class="col-12">
                <ol class="breadcrumb bg-transparent pl-0">
                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    <li class="breadcrumb-item active">New record</li>
                    <li class="breadcrumb-item active" aria-current="page">New record step 1.</li>
                </ol>
            </nav>
        </div>

        <div class="row">
            <div class="offset-md-2 col-md-4 border-top border-bottom text-center bg-color-primary rounded">
                <strong>1. Step</strong><br>
                Required information
            </div>

            <div class="col-md-4 border-top border-bottom text-center">
                <strong>2. Step</strong><br>
                Another information
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="offset-md-2 col-md-8">
                    <h1 class="d-inline-block mr-1 mr-md-3">1/2 New record</h1>
                    <p>Budú požadované len povinné informácie.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="bg-color-form pt-4 pb-4">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="offset-md-2 col-md-8">
                        <h2 class="mb-0">Required Information</h2>

                        <form>
                            <div class="bg-white border rounded p-3 mb-2">
                                <div class="form-group">
                                    <label for="primaryTaxon" class="custom-label-margin">* Taxon</label>
                                    <div class="row">
                                        <input type="text" class="form-control form-control-lg col-md-6 ml-3" id="primaryTaxon" placeholder="Primary taxon">
                                        <button type="button" class="btn btn-link" id="moreTaxon"><i class="fas fa-plus-circle"></i> More Taxon or Additional Taxon </button>
                                        <!-- povinne je zadat aspon jeden taxon -->
                                        <input type="text" class="form-control form-control-lg col-md-6 ml-3 mt-1" id="secondaryTaxon" placeholder="Paired Second Taxon ">
                                        <input type="text" class="form-control form-control-lg col-md-6 ml-3 mt-1" id="thirdTaxon" placeholder="Group Third Taxon (Polypoid) ">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="primaryTaxon" class="custom-label-margin">* Collectors</label>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <input type="text" class="form-control form-control-lg" id="collectors" placeholder="First collector, Second collector">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1" class="custom-label-margin">* Sequence in FASTA format:</label>
                                    <textarea class="form-control form-control-lg" id="exampleFormControlTextarea1" rows="3"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlFile1" class="custom-label-margin">
                                        <i data-toggle="tooltip" data-placement="top" title="AB1 file from sequencer"><u>* Upload File AB1F <i class="fas fa-info-circle"></i></u></i>
                                    </label>
                                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlFile1" class="custom-label-margin">* Upload File AB1R</label>
                                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlFile1" class="custom-label-margin">* Upload Image AB1F (pdf)</label>
                                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlFile1" class="custom-label-margin">* Upload Image AB1R (pdf)</label>
                                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                                </div>

                                <div>
                                    <label for="myTab" class="custom-label-margin">* Geography</label>
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item col-md-4 p-0 text-center">
                                            <a class="nav-link active " id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">River</a>
                                        </li>
                                        <li class="nav-item col-md-4 p-0 text-center">
                                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Self-sustaining stock</a>
                                        </li>
                                        <li class="nav-item col-md-4 p-0 text-center">
                                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Import trade</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active border border-top-0 rounded-bottom p-3" id="home" role="tabpanel" aria-labelledby="home-tab">
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1" class="custom-label-margin">* Country</label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <select class="form-control selectpicker custom-select custom-select custom-select-lg" id="exampleFormControlSelect1" data-live-search="true" title="Select country">
                                                            <option data-tokens="česko česká cesko">Czech republic</option>
                                                            <option data-tokens="Slovensko slovensko">Slovakia</option>
                                                            <option data-tokens="Nemecko">Germany</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1" class="custom-label-margin">* Main River Basin</label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <select class="form-control selectpicker custom-select custom-select custom-select-lg" id="exampleFormControlSelect1" data-live-search="true" title="Select river basin ">
                                                            <option data-tokens="Donau">Dunaj</option>
                                                            <option data-tokens="Morava">Morava</option>
                                                            <option data-tokens="Vltava">Vltava</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <label for="Latitude" class="col-12 custom-label-margin">* Concrete Locality  </label>
                                                <span class="col-12 d-inline">Na mape nájdite miesto odberu vzorky, umiestnite tam pointer a koordináty sa automaticky prepíšu. Druhou možnosťou je vložiť priamo GPS koordináty šírky a dĺžky.</span>

                                                <div class="col-md-8 mt-3">
                                                    <img src="public/img/ilustracne.png" alt="">
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="primaryTaxon" class="custom-label-margin mt-0">* Latitude</label>
                                                        <input type="text" class="form-control custom-select-lg" id="Latitude" placeholder="Eg. 49.1935686 ">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="primaryTaxon" class="custom-label-margin">* Longitude</label>
                                                        <input type="text" class="form-control custom-select-lg" id="Longitude" placeholder="Eg. 16.6051327">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade border border-top-0 rounded-bottom p-3" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                            <!-- Inputy som tu nechal zamerne: copmany bude potrebne upravit do noramlizovaneho tvaru aby
                                            sa nestavalo, ze bude pridana firma s nazvo s.r.o copmany a companny s.r.o ale select box nedava zmysel
                                            lebo nie je mozne dopredu prirpavit zoznam firem - normalizacia moze nastat pri kontrole vkladaneho zaznamu administratorom
                                            -->

                                            <div class="form-group">
                                                <label for="primaryTaxon" class="custom-label-margin">* Company</label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control form-control-lg" id="collectors" placeholder="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="primaryTaxon" class="custom-label-margin">* Pond/Hatchery/Broodstock</label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control form-control-lg" id="collectors" placeholder="Name and number">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="primaryTaxon" class="custom-label-margin">* Business name/strain</label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control custom-select-lg" id="primaryTaxon" placeholder="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="primaryTaxon" class="custom-label-margin">Comment</label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control custom-select-lg" id="primaryTaxon" placeholder="">
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <label for="Latitude" class="col-12 custom-label-margin">* Concrete Locality </label>
                                                <span class="col-12 d-inline">Na mape nájdite miesto odberu vzorky, umiestnite tam pointer a koordináty sa automaticky prepíšu. Druhou možnosťou je vložiť priamo GPS koordináty šírky a dĺžky.</span>

                                                <div class="col-md-8 mt-3">
                                                    <img src="public/img/ilustracne.png" alt="">
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="primaryTaxon" class="custom-label-margin mt-0">* Latitude</label>
                                                        <input type="text" class="form-control form-control-lg" id="primaryTaxon" placeholder="Eg. 49.1935686">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="primaryTaxon" class="custom-label-margin">* Longitude</label>
                                                        <input type="text" class="form-control form-control-lg" id="primaryTaxon" placeholder="Eg. 16.6051327">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade border border-top-0 rounded-bottom p-3" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1" class="custom-label-margin">* Country</label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <select class="form-control selectpicker custom-select custom-select custom-select-lg" id="exampleFormControlSelect1" data-live-search="true" title="Select country">
                                                            <option data-tokens="česko česká cesko">Czech republic</option>
                                                            <option data-tokens="Slovensko slovensko">Slovakia</option>
                                                            <option data-tokens="Nemecko">Germany</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="primaryTaxon" class="custom-label-margin">* Locality (City / Region)</label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control custom-select-lg" id="primaryTaxon" placeholder="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="primaryTaxon" class="custom-label-margin">* Company</label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control custom-select-lg" id="primaryTaxon" placeholder="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="primaryTaxon" class="custom-label-margin">* Business name/strain</label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control custom-select-lg" id="primaryTaxon" placeholder="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="d-flex align-items-center mt-4">
                                <p class="flex-grow-1"><strong>Krok: 1 z 2</strong></p>
                                <a class="btn button-secondary-color btn-lg" href="new-record-step-2.php" role="button">Pokracovat k nepovinnym informaciam <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>

</main>


<?php require 'about-us.html'; ?>
<?php require 'footer.html'; ?>
<?php require 'page-list.html'; ?>

<script type="text/javascript" src="public/node_modules/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="public/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="public/node_modules/toastr/toastr.js"></script>
<script type="text/javascript" src="public/node_modules/nette.ajax.js/nette.ajax.js"></script>
<script type="text/javascript" src="public/node_modules/nette-forms/src/assets/netteForms.js"></script>
<script type="text/javascript" src="public/node_modules/live-form-validation/live-form-validation.js"></script>
<script type="text/javascript" src="public/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="public/js/script.js"></script>

</body>
</html>
