
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Language" content="cs">
    <meta name="created" content="Liquid Design s.r.o.">
    <link rel="shortcut icon" href="/jjtmpublic/favicon.ico">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="public/node_modules/normalize.css/normalize.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="public/node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="public/node_modules/@fortawesome/fontawesome-free/css/all.css" rel="stylesheet" type="text/css">
    <link href="public/css/lightbox.min.css" rel="stylesheet" type="text/css">
    <link href="public/css/base.css" rel="stylesheet" type="text/css">
    <link href="public/css/front.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">

    <!--[if its IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>
<body>

<?php require 'menu-after-login.html'; ?>

<main class="bg-light">
    <div class="container">
        <div class="row">
            <nav aria-label="breadcrumb" class="col-12">
                <ol class="breadcrumb bg-transparent pl-0">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Library</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Data</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-12">
                <h1>Search result</h1>
            </div>
        </div>
    </div>

    <div class="bg-color-form pb-3">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?php require 'search-filter.html'; ?>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <h2>25 Result: <small>2 Country, 1 Taxon, 2 Standard</small> </h2>
                </div>
            </div>

            <div class="row">
                <div class="col-12 ">
                    <img src="public/img/mapa.png" alt="mapa" class="rounded mx-auto d-block img-fluid">
                </div>
            </div>

            <div class="row">
                <div class="col-12 mt-4 mb-4">
                    <div class="bg-color-primary pb-3 rounded text-center bg-shadow background-gradient">
                        <h2 class="col-12 mb-0 mt-1 text-white">Top Similarity: 100%</h2>
                        <p class="col-12 mt-0 big-text text-white">Localities with 100% Similarity:</p>

                        <ul class="list-unstyled d-flex flex-wrap justify-content-center">
                            <li class="col-md-4 mb-3"><img src="public/img/czech-republic.png" alt="czech" style="max-width: 24px;"> <strong>CZE</strong>, Vltava, <span>(Rieka)</span></li>
                            <li class="col-md-4 mb-3"><img src="public/img/czech-republic.png" alt="czech" style="max-width: 24px;"> <strong>CZE</strong>, Liquid Fish s.r.o., <span>(Vlastny chov)</span></li>
                            <li class="col-md-4 mb-3"><img src="public/img/germany.png" alt="germany" style="max-width: 24px;"> <strong>GER</strong>, Berlin <span>(Zahraničie)</span></li>
                            <li class="col-md-4 mb-3"><img src="public/img/germany.png" alt="germany" style="max-width: 24px;"> <strong>GER</strong>, Berlin <span>(Zahraničie)</span></li>
                        </ul>
                        <div class="d-flex flex-column">
                            <p class="text-center big-text text-white">Našli ste vzorku aj inde? Nahrajte ju, zaberie to len pár sekúnd a veľmi nám tým pomôžete</p>
                            <div class="text-center"><a href="new-location.php" class=""><button type="button" class="btn button-primary-color">Insert New Locality</button></a></div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- alternative result -->
            <!--
            <div class="row">
                <div class="col-12 mt-3">
                    <div class="bg-color-primary pb-3 rounded text-center">
                        <h3 class="col-12 mb-0 mt-1">Najvyssia najdena zhoda: 92%</h3>
                        <p class="col-12 mt-0">Našli sme len podobné druhy, vaša vzorka je unikátna, pomôžte nám rozšíriť databázu a nahrajte vzorku do databázy</p>

                        <div class="d-flex flex-column">
                            <div class="text-center"><a href="new-location.php" class=""><button type="button" class="btn btn-secondary">Pridať novu vzorku</button></a></div>
                        </div>
                    </div>
                </div>
            </div>
            -->

            <div class="row complet-report">
                <div class="col-12 mt-3">
                    <div class="table-responsive-xl">
                        <table class="table">
                            <thead class="">
                            <tr class="bg-shadow">
                                <th scope="col">
                                    <div class="left-box pl-2 d-flex align-items-center bg-color-primary">
                                        <button class="unsellect-all btn button-primary-color-outline btn-sm"><i class="fas fa-times ml-1 mr-1"></i>Unsellect All</button>
                                    </div>
                                </th>
                                <th scope="col">
                                    <div class="pl-2 d-flex align-items-stretch bg-color-primary">
                                        <span class="d-flex align-items-center">Country</span>
                                    </div>
                                </th>
                                <th scope="col">
                                    <div class="pl-2 d-flex align-items-stretch bg-color-primary">
                                        <span class="d-flex align-items-center">Location</span>
                                    </div>
                                </th>
                                <th scope="col">
                                    <div class="pl-2 d-flex align-items-stretch bg-color-primary">
                                        <span class="d-flex align-items-center">Created</span>
                                    </div>
                                </th>
                                <th scope="col">
                                    <div class="pl-2 d-flex align-items-stretch bg-color-primary">
                                        <span class="d-flex align-items-center">ID</span>
                                    </div>
                                </th>
                                <th scope="col">
                                    <div class="pl-2 d-flex align-items-stretch bg-color-primary">
                                        <span class="d-flex align-items-center">Foto</span>
                                    </div>
                                </th>
                                <th scope="col">
                                    <div class="pl-2 d-flex align-items-stretch bg-color-primary">
                                        <span class="d-flex align-items-center">Cover</span>
                                    </div>
                                </th>
                                <th scope="col">
                                    <div class="pl-2 d-flex align-items-stretch bg-color-primary">
                                        <span class="d-flex align-items-center">Match</span>

                                    </div>
                                </th>
                                <th scope="col">
                                    <div class="right-box pl-2 d-flex align-items-center justify-content-end bg-color-primary">
                                        <button type="button" class="btn button-primary-color-outline btn-sm mr-2"><i class="fas fa-download"></i> All (15)</button>
                                        <button type="button" class="btn button-primary-color btn-sm mr-2" disabled id="selected-export"><i class="fas fa-download"></i> Sellected (<span id="selected-number">0</span>)</button>
                                    </div>
                                </th>
                            </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <td scope="row" class="row-with-checkbox">
                                        <div class="left-box rounded-left mt-2 pl-2 bg-white d-flex align-items-stretch">
                                            <label class="d-flex align-items-stretch">
                                                <input type="checkbox" name="check" class="">
                                                <span class="label-text align-self-center pt-1 pb-1">Phoxinus phoxinus lineage</span>
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="center-box mt-2 pl-2 bg-white d-flex align-items-stretch">
                                            <span class="align-self-center"><i class="far fa-flag"></i> 1x</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                            <span class="align-self-center"><i class="fas fa-map-marker-alt"></i> 3x</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                            <span class="align-self-center">5.8.2017</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                            <span class="align-self-center">#12312</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                            <span class="align-self-center"><i class="far fa-image"></i> 2x</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                            <span class="align-self-center"> 99% </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                            <span class="align-self-center"> 98.4% </span>
                                        </div>
                                    </td>
                                    <td class="">
                                        <div class="right-box rounded-right mt-2 pl-2 pr-2 bg-white d-flex align-items-center justify-content-end">
                                            <button type="button" class="btn button-secondary-color-outline open-row">Alignment <i class="fas fa-chevron-down"></i></button>
                                            <button type="button" class="btn button-secondary-color-outline ml-1 mr-1"><i class="fas fa-download"></i></button>
                                            <a href="record-detail.php"><button type="button" class="btn button-secondary-color">Detail <i class="fas fa-chevron-right"></i></button></a>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="close-row">
                                    <td colspan="9">
                                        <div class="close-row-box rounded-bottom bg-white p-2">
                                            <p class="col-12 mt-0 text-center">Range: 191 to 784, Gaps:0</p>
                                            <img src="public/img/rebrik.png" alt="rebrik" class="mx-auto d-block">
                                            <div class="mt-2 mb-2 d-flex justify-content-end align-items-end">
                                                <button type="button" class="btn button-secondary-color-outline btn-sm close-row-button">Close <i class="fas fa-chevron-up"></i></button>
                                                <button type="button" class="btn button-secondary-color-outline btn-sm ml-1 mr-1"><i class="fas fa-download"></i></button>
                                                <a href="record-detail.php"><button type="button" class="btn button-secondary-color btn-sm">Detail <i class="fas fa-chevron-right"></i></button></a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td scope="row" class="row-with-checkbox">
                                        <div class="left-box rounded-left mt-2 pl-2 bg-white d-flex align-items-stretch">
                                            <label class="d-flex align-items-center">
                                                <input type="checkbox" name="check" class="">
                                                <span class="label-text">Phoxinus phoxinus lineage</span>
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="center-box mt-2 pl-2 bg-white d-flex align-items-stretch">
                                            <span class="align-self-center"><i class="far fa-flag"></i> 1x</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                            <span class="align-self-center"><i class="fas fa-map-marker-alt"></i> 3x</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                            <span class="align-self-center">5.8.2017</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                            <span class="align-self-center">#12312</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                            <span class="align-self-center"><i class="far fa-image"></i> 2x</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                            <span class="align-self-center"> 99% </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                            <span class="align-self-center"> 98.4% </span>
                                        </div>
                                    </td>
                                    <td class="">
                                        <div class="right-box rounded-right mt-2 pl-2 pr-2 bg-white d-flex align-items-center justify-content-end">
                                            <button type="button" class="btn button-secondary-color-outline open-row">Alignment <i class="fas fa-chevron-down"></i></button>
                                            <button type="button" class="btn button-secondary-color-outline ml-1 mr-1"><i class="fas fa-download"></i></button>
                                            <a href="record-detail.php"><button type="button" class="btn button-secondary-color">Detail <i class="fas fa-chevron-right"></i></button></a>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="close-row">
                                    <td colspan="9">
                                        <div class="close-row-box rounded-bottom bg-white p-2">
                                            <p class="col-12 mt-0 text-center">Range: 191 to 784, Gaps:0</p>
                                            <img src="public/img/rebrik.png" alt="rebrik" class="mx-auto d-block">
                                            <div class="mt-2 mb-2 d-flex justify-content-end align-items-end">
                                                <button type="button" class="btn button-secondary-color-outline btn-sm close-row-button">Close <i class="fas fa-chevron-up"></i></button>
                                                <button type="button" class="btn button-secondary-color-outline btn-sm ml-1 mr-1"><i class="fas fa-download"></i></button>
                                                <a href="record-detail.php"><button type="button" class="btn button-secondary-color btn-sm">Detail <i class="fas fa-chevron-right"></i></button></a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td scope="row" class="row-with-checkbox">
                                        <div class="left-box rounded-left mt-2 pl-2 bg-white d-flex align-items-stretch">
                                            <label class="d-flex align-items-center">
                                                <input type="checkbox" name="check" class="">
                                                <span class="label-text">Phoxinus phoxinus lineage</span>
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="center-box mt-2 pl-2 bg-white d-flex align-items-stretch">
                                            <span class="align-self-center"><i class="far fa-flag"></i> 1x</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                            <span class="align-self-center"><i class="fas fa-map-marker-alt"></i> 3x</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                            <span class="align-self-center">5.8.2017</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                            <span class="align-self-center">#12312</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                            <span class="align-self-center"><i class="far fa-image"></i> 2x</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                            <span class="align-self-center"> 99% </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="center-box pl-2 mt-2 bg-white d-flex align-items-stretch">
                                            <span class="align-self-center"> 98.4% </span>
                                        </div>
                                    </td>
                                    <td class="">
                                        <div class="right-box rounded-right mt-2 pl-2 pr-2 bg-white d-flex align-items-center justify-content-end">
                                            <button type="button" class="btn button-secondary-color-outline open-row">Alignment <i class="fas fa-chevron-down"></i></button>
                                            <button type="button" class="btn button-secondary-color-outline ml-1 mr-1"><i class="fas fa-download"></i></button>
                                            <a href="record-detail.php"><button type="button" class="btn button-secondary-color">Detail <i class="fas fa-chevron-right"></i></button></a>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="close-row">
                                    <td colspan="9">
                                        <div class="close-row-box rounded-bottom bg-white p-2">
                                            <p class="col-12 mt-0 text-center">Range: 191 to 784, Gaps:0</p>
                                            <img src="public/img/rebrik.png" alt="rebrik" class="mx-auto d-block">
                                            <div class="mt-2 mb-2 d-flex justify-content-end align-items-end">
                                                <button type="button" class="btn button-secondary-color-outline btn-sm close-row-button">Close <i class="fas fa-chevron-up"></i></button>
                                                <button type="button" class="btn button-secondary-color-outline btn-sm ml-1 mr-1"><i class="fas fa-download"></i></button>
                                                <a href="record-detail.php"><button type="button" class="btn button-secondary-color btn-sm">Detail <i class="fas fa-chevron-right"></i></button></a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 mt-2">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1">Previous</a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#">Next</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>


        </div>
    </div>
</main>

<?php require 'about-us.html'; ?>
<?php require 'footer.html'; ?>
<?php require 'page-list.html'; ?>

<div class="select-table-row d-none" id="export-box">
    <div class="container">
        <div class="row pt-3 pb-3">
            <div class="col-12 d-flex align-items-center">
                <div class="label-text pl-2">Vybraných záznamov: <span id="selected-number-bottom">0</span></div>

                <div class="ml-auto pr-2">
                    <button type="button" class="btn btn-outline-light unsellect-all"><i class="fas fa-times ml-1 mr-1"></i>Select none</button></button>
                    <button type="button" class="btn button-secondary-color"><i class="fas fa-download"></i> Export selected record</button>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript" src="public/node_modules/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="public/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="public/node_modules/toastr/toastr.js"></script>
<script type="text/javascript" src="public/node_modules/nette.ajax.js/nette.ajax.js"></script>
<script type="text/javascript" src="public/node_modules/nette-forms/src/assets/netteForms.js"></script>
<script type="text/javascript" src="public/node_modules/live-form-validation/live-form-validation.js"></script>
<script type="text/javascript" src="public/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="public/js/script.js"></script>

</body>
</html>
